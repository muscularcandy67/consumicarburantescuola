package Carburante;

public class Distributore {

    private double costoLitro;

    public Distributore(double costoLitro) {
        setCostoLitro(costoLitro);
    }


    public double getCostoLitro() {
        return costoLitro;
    }

    public boolean setCostoLitro(double costoLitro) {
        if (costoLitro >= 0) {
            this.costoLitro = costoLitro;
            return true;
        }
        return false;
    }

    public String addFuel(Serbatoio s){
        double l = s.getCapienza() - s.getCarburante();
        s.addCarburante(s.getCapienza()-s.getCarburante());
        return "Il pieno è stato effettuato al costo di: "+ l*costoLitro;
    }

    public String addFuel (Serbatoio s, double importo){
        double l=importo/costoLitro;
        double p = s.addCarburante(l);
        if(p==0.0){
            return "Il rifornimento è stato effettuato per l'importo desiderato";
        }
        else{
            return "Il rifornimento è stato effettuato in modo parziale, l'importo residuo è " + (l-p);
        }

    }
}
