package Carburante;

public class ConsumiCarburante {
    private double livelloCarburante;
    private double litriKm;

    public ConsumiCarburante(double livelloCarburante, double litriKm) {
        setLivelloCarburante(livelloCarburante);
        setLitriKm(litriKm);
    }

    public double getLivelloCarburante() {
        return livelloCarburante;
    }

    public boolean setLivelloCarburante(double livelloCarburante) {
        if (livelloCarburante >= 0) {
            this.livelloCarburante = livelloCarburante;
            return true;
        } else return false;
    }

    public double getLitriKm() {
        return litriKm;
    }

    public boolean setLitriKm(double litriKm) {
        if (litriKm > 0) {
            this.litriKm = litriKm;
            return true;
        } else return false;
    }

    public double calcolaConsumo(double km) {
        if (km > 0) {
            return km * litriKm;
        } else return -1;
    }
}
