package Carburante;

public class Serbatoio {

    private final double capienza;
    private ConsumiCarburante c;


    public Serbatoio(double litriKm, double capienza) {
        c = new ConsumiCarburante(0, litriKm);
        this.capienza = capienza;
    }

    public double addCarburante(double quantita) {
        if (quantita >= 0) {
            if ((c.getLivelloCarburante() + quantita) <= capienza) {
                c.setLivelloCarburante(c.getLivelloCarburante() + quantita);
                return 0.0;
            } else {
                double p = ((c.getLivelloCarburante() + quantita) - capienza);
                c.setLivelloCarburante(capienza);
                return p;
            }
        }
        return -1;
    }

    public double getCarburante() {
        return c.getLivelloCarburante();
    }

    public double getCapienza() {
        return capienza;
    }
}
